from pony.orm import db_session, count
from starlette.middleware.cors import CORSMiddleware

from database import FormOrigin


class DynamicCORSMiddleware(CORSMiddleware):
    def is_allowed_origin(self, origin: str) -> bool:
        with db_session:
            result = count(f for f in FormOrigin if f.origin == origin)  # type: ignore
            return result > 0
