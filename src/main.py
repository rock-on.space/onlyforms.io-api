from fastapi import FastAPI

from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.util import get_remote_address
from slowapi.middleware import SlowAPIASGIMiddleware
from slowapi.errors import RateLimitExceeded

import v1
from config import settings
from cors_middleware import DynamicCORSMiddleware
from security_headers_middleware import SecurityHeadersMiddleware

app = FastAPI()

app.add_middleware(SecurityHeadersMiddleware)
app.add_middleware(
    DynamicCORSMiddleware,
    allow_credentials=False,
    allow_methods=["POST", "GET"],
    allow_headers=["*"],
    max_age=3600,
)

limiter = Limiter(key_func=get_remote_address, default_limits=["1/2seconds"])
app.state.limiter = limiter
app.add_exception_handler(RateLimitExceeded, _rate_limit_exceeded_handler)
app.add_middleware(SlowAPIASGIMiddleware)

app.include_router(v1.router)


@app.get("/healthz")
async def healthz():
    return {"message": "Hello World"}


@app.get("/alive")
async def alive():
    return {"message": "Hello World"}
