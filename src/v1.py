from datetime import datetime

from fastapi import APIRouter, Request, HTTPException
from pony.orm import db_session

from database import field_type_validator, insert_response
from database import get_form as get_form_db, get_origin as get_origin_db
from database import Form as FormDB


router = APIRouter(
    prefix="/v1",
    tags=["v1"],
)


@router.post(
    "/f/{owner_id}/{form_id}",
    tags=["forms"],
    responses={
        404: {"description": "Form Not Found"},
        400: {"description": "Invalid submission"},
        201: {"description": "Accepted"},
    },
    status_code=201,
)
async def post_form_submission(owner_id: str, form_id: str, req: Request):
    with db_session:
        # Find the form
        form_db = await get_form_db(owner_id, form_id)
        if form_db is None:
            raise HTTPException(status_code=404, detail="Form not found")
        # Validate the origin
        origin_db = await get_origin_db(form_db, req.headers.get("Origin"))
        if origin_db is None:
            raise HTTPException(status_code=412, detail="Invalid Origin")
        # Find the data
        if req.headers.get("Content-Type").startswith("multipart/form-data"):
            raw_data = await req.form(max_files=0, max_fields=len(form_db.fields))
        elif req.headers.get("Content-Type").startswith(
            "application/x-www-form-urlencoded"
        ):
            raw_data = await req.form(max_files=0, max_fields=len(form_db.fields))
        elif req.headers.get("Content-Type").startswith("application/json"):
            raw_data = await req.json()
        else:
            raise HTTPException(status_code=400, detail="Unsupported Content-Type")
        # Clean the data
        clean_data = await bleach_data(raw_data, form_db)
        await insert_response(form_db, datetime.now(), clean_data)
        return ""


async def bleach_data(raw_data: dict, form_db: FormDB) -> dict:
    clean_data = {}
    for field in form_db.fields:
        data = raw_data.get(field.name)

        if field.name not in raw_data.keys() and field.required is True:
            raise HTTPException(status_code=400, detail="Missing data")
        if field.name not in raw_data.keys() and field.required is False:
            continue

        if (data := field_type_validator(field.type, data)) is None:
            raise HTTPException(status_code=400, detail="Invalid data type")

        clean_data[field.name] = data

    if set(raw_data.keys()) != set(clean_data.keys()):
        raise HTTPException(status_code=400, detail="Unexpected data submitted")
    return clean_data
