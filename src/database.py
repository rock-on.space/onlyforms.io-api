from datetime import datetime
from enum import StrEnum, auto

import bleach
import pendulum
from pendulum.parsing import ParserError
from pony.orm import Database, select, commit
from pony.orm import Required as PonyRequired
from pony.orm import PrimaryKey as PonyPrimaryKey
from pony.orm import Set as PonySet
from pony.orm import Json as PonyJson
from pony.orm import composite_key as pony_composite_key

from config import settings

db = Database()


class AcceptableType(StrEnum):
    string = auto()
    number = auto()
    datetime = auto()


class Owner(db.Entity):  # type: ignore
    uid = PonyPrimaryKey(str)
    full_name = PonyRequired(str)
    email = PonyRequired(str)
    forms = PonySet("Form")


class Form(db.Entity):  # type: ignore
    uid = PonyPrimaryKey(str)
    owner = PonyRequired(Owner)
    responses = PonySet("FormResponse")
    fields = PonySet("FormField")
    origins = PonySet("FormOrigin")
    pony_composite_key(uid, owner)


class FormOrigin(db.Entity):  # type: ignore
    form = PonyRequired(Form)
    origin = PonyRequired(str)


class FormField(db.Entity):  # type: ignore
    form = PonyRequired(Form)
    name = PonyRequired(str)
    type = PonyRequired(AcceptableType)
    required = PonyRequired(bool)


class FormResponse(db.Entity):  # type: ignore
    form = PonyRequired(Form)
    datestamp = PonyRequired(datetime)
    response = PonyRequired(PonyJson)


async def get_form(owner_uid: str, form_uid: str) -> Form:
    form = select(
        # SELECT "f".*
        f
        # FROM "Owner" "o", "Form" "f"
        for o in Owner  # type: ignore
        for f in Form  # type: ignore
        # WHERE "f"."owner" = "o"."uid"
        if f.owner == o
        # AND "o".uid = ?
        and o.uid == owner_uid
        # AND "f".uid = ?
        and f.uid == form_uid
    ).first()  # ORDER BY 1 LIMIT 1
    return form


async def get_origin(form: Form, origin_header: str) -> FormOrigin:
    origin = select(
        o
        for o in FormOrigin  # type: ignore
        if o.form == form and o.origin == origin_header
    ).first()
    return origin


async def insert_response(form: Form, datestamp: datetime, response: dict):
    FormResponse(form=form, datestamp=datestamp, response=response)
    commit()


def field_type_validator(field_type: AcceptableType, data: object) -> object:
    """
    Tests to see if the data conforms to the expected type.

    :param AcceptableType field_type: The field type to test against
    :param object data: The data to validate
    :return bool: The data is valid or not
    """
    match field_type:
        case AcceptableType.string:
            if isinstance(data, str):
                data = bleach.clean(data)
                return data
            return None
        case AcceptableType.number:
            if isinstance(data, str):
                try:
                    if "." in data:
                        data = float(data)
                    else:
                        data = int(data)
                except ValueError:
                    return None
            if isinstance(data, (int, float)):
                return data
            return None
        case AcceptableType.datetime:
            if isinstance(data, str):
                try:
                    data = pendulum.parse(data)
                except ParserError:
                    return None
            if isinstance(data, datetime):
                return data.isoformat()
            return None
        case _:
            return None


match settings.db_provider:
    case "sqlite":
        db.bind(
            provider=settings.db_provider, filename=settings.db_name, create_db=True
        )
    case "postgres":
        db.bind(
            provider=settings.db_provider,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_username,
            password=settings.db_password,
            database=settings.db_name,
            sslmode=settings.db_sslmode,
        )
    case "cockroach":
        db.bind(
            provider=settings.db_provider,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_username,
            password=settings.db_password,
            database=settings.db_name,
            sslmode=settings.db_sslmode,
        )
    case _:
        raise ValueError("Unknown database provider")

db.generate_mapping(create_tables=False)
