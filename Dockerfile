FROM registry.gitlab.com/rock-on.space/base-image:v1.0.3

RUN pip install --upgrade pip
RUN pip install poetry
ADD ./poetry.lock /src/poetry.lock
ADD ./pyproject.toml /src/pyproject.toml
RUN cd src; poetry config virtualenvs.create false; poetry install
ADD ./src /src
WORKDIR /src

EXPOSE 8080

CMD python -m uvicorn main:app --host 0.0.0.0 --port 8080

